-- Some changes to the PostgreSQL database with the intention to have it work faster.
-- christian.bauer@med.uni-goettingen.de

-- create new INDEX (efficiency checked)
CREATE INDEX observation_fact_CONCEPT_CD ON i2b2demodata.observation_fact (CONCEPT_CD);
CREATE INDEX i2b2_C_BASECODE ON i2b2metadata.i2b2 (C_BASECODE);
CREATE INDEX i2b2_C_DIMCODE ON i2b2metadata.i2b2 (C_DIMCODE);

-- replace the original and slow view which uses the length of c_fullname
CREATE OR REPLACE VIEW i2b2metadata.i2b2_trial_nodes AS
   SELECT DISTINCT ON (i2b2.c_comment) i2b2.c_fullname,
    "substring"(i2b2.c_comment, 7) AS trial
   FROM i2b2metadata.i2b2
  WHERE i2b2.c_comment IS NOT NULL AND i2b2.c_hlevel = 1::numeric
  ORDER BY i2b2.c_comment;
